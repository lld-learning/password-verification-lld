package verification_test;

import org.ashish.verify.exceptions.InvalidPasswordException;
import org.ashish.verify.service.PasswordVerificationService;
import org.ashish.verify.validation.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class PasswordTest {
    PasswordVerificationService verificationService;
    @BeforeEach
    void setup(){
        CapitalCharacterValidation capitalCharacterValidation=new CapitalCharacterValidation();
        CharacterCountValidation characterCountValidation=new CharacterCountValidation();
        InvalidCharacterValidation invalidCharacterValidation=new InvalidCharacterValidation();
        NumericValidation numericValidation=new NumericValidation();
        StartCharacterValidation startCharacterValidation=new StartCharacterValidation();

        List<Validation> validationList=new ArrayList<>();
        validationList.add(characterCountValidation);
        validationList.add(numericValidation);
        validationList.add(capitalCharacterValidation);
        validationList.add(invalidCharacterValidation);
        validationList.add(startCharacterValidation);

        verificationService=new PasswordVerificationService(validationList);
    }

    @Test
    void characterCountLessValidation(){
        String password="abc";
        Assertions.assertFalse(verificationService.validate(password));
    }

    @Test
    void numberPresentValidation(){
        String password="abcd";
        Assertions.assertFalse(verificationService.validate(password));
    }

    @Test
    void capitalCharacterValidation(){
        String password="abc12";
        Assertions.assertFalse(verificationService.validate(password));
    }

    @Test
    void invalidCharacterValidation(){
        String password="abc12D//";
        Assertions.assertFalse(verificationService.validate(password));
    }

    @Test
    void startCharacterCountValidation(){
        String password="1abcD12";
        Assertions.assertFalse(verificationService.validate(password));
    }

    @Test
    void passwordValidValidation(){
        String password="abcD12";
        Assertions.assertTrue(verificationService.validate(password));
    }
}

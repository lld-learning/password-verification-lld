package org.ashish.verify.validation;

public class CharacterCountValidation implements Validation{

    private static int maxCount=3;
    @Override
    public boolean validate(String password) {
        if(password==null)
            return false;
        //check for at least 4 characters
        return password.length()>3;
    }
}

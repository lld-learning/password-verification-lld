package org.ashish.verify.validation;

import org.ashish.verify.exceptions.InvalidPasswordException;

public interface Validation {
    boolean validate(String password) throws InvalidPasswordException;
}

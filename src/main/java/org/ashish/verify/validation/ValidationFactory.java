package org.ashish.verify.validation;

import java.util.ArrayList;
import java.util.List;

public class ValidationFactory {
    private static List<Validation> validationList;

    public static List<Validation> initialize(){
        if(validationList!=null && !validationList.isEmpty())
            return validationList;

        CapitalCharacterValidation capitalCharacterValidation=new CapitalCharacterValidation();
        CharacterCountValidation characterCountValidation=new CharacterCountValidation();
        InvalidCharacterValidation invalidCharacterValidation=new InvalidCharacterValidation();
        NumericValidation numericValidation=new NumericValidation();
        StartCharacterValidation startCharacterValidation=new StartCharacterValidation();

        validationList=new ArrayList<>();
        validationList.add(characterCountValidation);
        validationList.add(numericValidation);
        validationList.add(capitalCharacterValidation);
        validationList.add(invalidCharacterValidation);
        validationList.add(startCharacterValidation);
        return validationList;
    }
}

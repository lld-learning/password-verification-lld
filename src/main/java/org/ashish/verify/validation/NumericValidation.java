package org.ashish.verify.validation;

public class NumericValidation implements Validation{
    @Override
    public boolean validate(String password) {
        if(password==null)
            return false;
        //check if the password contains a number
        char[] chars = password.toCharArray();
        for(char c : chars){
            if(Character.isDigit(c)){
                return true;
            }
        }
        return false;
    }
}

package org.ashish.verify.validation;

import org.ashish.verify.exceptions.InvalidPasswordException;

public class CapitalCharacterValidation implements Validation{
    @Override
    public boolean validate(String password) throws InvalidPasswordException {
        if(password==null)
            return false;
        char[] chars = password.toCharArray();
        for(char c : chars){
            if(Character.isUpperCase(c)){
                return true;
            }
        }
        return false;
    }
}

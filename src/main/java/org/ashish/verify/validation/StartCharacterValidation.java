package org.ashish.verify.validation;

import org.ashish.verify.exceptions.InvalidPasswordException;

public class StartCharacterValidation implements Validation{
    @Override
    public boolean validate(String password) throws InvalidPasswordException {
        if(password==null)
            return false;
        //check if the first character is a number
        return !Character.isDigit(password.charAt(0));

    }
}

package org.ashish.verify.validation;

import org.ashish.verify.exceptions.InvalidPasswordException;

public class InvalidCharacterValidation implements Validation{

    @Override
    public boolean validate(String password) throws InvalidPasswordException {
        if(password==null)
            return false;
        //check for spaces or slashes
        return !(password.contains(" ") || password.contains("/"));

    }
}

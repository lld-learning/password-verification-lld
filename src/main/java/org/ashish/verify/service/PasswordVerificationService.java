package org.ashish.verify.service;

import org.ashish.verify.exceptions.InvalidPasswordException;
import org.ashish.verify.validation.Validation;

import java.util.List;

public class PasswordVerificationService {

    private final List<Validation> validationList;

    public PasswordVerificationService(List<Validation> validationList) {
        this.validationList = validationList;
    }

    public boolean validate(List<String> validations, String password) throws InvalidPasswordException {
        boolean validPasword=true;
        for(Validation validation:validationList){
            //validates the password whatever is the logic in individual validations
            validPasword=validation.validate(password);
            if(!validPasword)
                break;
        }
        return validPasword;
    }
}
